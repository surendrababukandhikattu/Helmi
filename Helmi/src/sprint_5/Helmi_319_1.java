package sprint_5;

	import java.io.File;

import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

	public class Helmi_319_1 {

		public static void main(String[] args) throws Exception 
		{
			
			System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
			WebDriver driver= new ChromeDriver();
			driver.get("https://stresstest.helmi.fi");		
			
			 driver.findElement(By.name("username")).sendKeys("superuser");
			 driver.findElement(By.name("password")).sendKeys("superuser");
			 driver.findElement(By.className("buttonLogin")).click();
			 driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

//Slecting the laguage			
			 driver.findElement(By.xpath("//*[@class='last']/span/a[1]/img")).click();
			 
			 Select dropdown = new Select(driver.findElement(By.id("runtimeSessionFaculty")));
			 		dropdown.selectByVisibleText("Alakoulu");
//clicking on Lukkari			
			 driver.findElement(By.xpath("//*[@class='valikko']/li[7]/a/span")).click();
			 driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		
	//driver.findElement(By.xpath("//*[@id='plans-list']/ui-view/div/div[2]/div[2]/table/tbody/tr[6]/td[1]/a")).click();	 
			 
	//WebDriverWait wait = new WebDriverWait(driver, 200);
	// WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='plans-list']/ui-view/div/div[1]/div[2]/select")));
	//element.click();

				
			/*Select dropdown1 = new Select(driver.findElement(By.xpath("//*[@id='plans-list']/ui-view/div/div[1]/div[2]/select")));
					dropdown1.selectByVisibleText("Luo uusi suunnitelma");		*/	
		
//Selecting the existing timetable	
			 driver.findElement(By.xpath("//a[@href='/timetableplan/138']")).click();
//VIIKKORYTMIT.CLICK			 
			driver.findElement(By.xpath("//*[@class='left-menu']/ul/li[3]/a[1]")).click();
			/*driver.findElement(By.cssSelector(".left-menu a")).click();
			driver.findElement(By.cssSelector(".left-menu li")).click();
			driver.findElement(By.cssSelector("#left-menu > li:nth-child(3) a")).click(); 
			driver.findElement(By.cssSelector(".left-menu  li(3)")).click();*/ 
			
//Oletus.Click
			driver.findElement(By.xpath("//*[@class='list']/tbody/tr/td/a")).click();
//Seleting time period
	Select  dropdown2 = new Select(driver.findElement(By.xpath("//*[@id='weekly-pattern-settings']/form/div[1]/label[2]/select")));
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);		
			dropdown2.selectByVisibleText("75 min");			
//Timetable template click
			driver.findElement(By.xpath("//*[@id='weekly-pattern']/tbody/tr/td[2]/div/div[9]")).click();
			driver.findElement(By.xpath("//*[@id='weekly-pattern']/tbody/tr/td[2]/div/div[2]")).click();	 
//Screenshots			
			TakesScreenshot ts=(TakesScreenshot)driver;
			File source=ts.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(source, new File("./Screenshots/Timetable.png"));
			System.out.println("screenshot taken");
		}	
	}

