package login;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.JavascriptExecutor;


public class Editing_studntprofile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
		WebDriver driver= new ChromeDriver();
		
driver.get("https://stresstest.helmi.fi");
	//driver.manage().window().maximize();
	 driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
// Application login	
	 driver.findElement(By.name("username")).sendKeys("superuser");
	 driver.findElement(By.name("password")).sendKeys("superuser");
	 driver.findElement(By.className("buttonLogin")).click();
	 driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

//Changing the language to English
	 driver.findElement
     (By.xpath (".//*[@id='menupalkki']/div/ul[1]/li[10]/span/a[3]/img")).click();

//Selecting from drop down for Educational Institution
	Select dropdown = new Select(driver.findElement(By.id("runtimeSessionFaculty")));
	dropdown.selectByVisibleText("Alakoulu");
	 
	driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	 
// Clicking on Student module
	 driver.findElement(By.xpath("//*[@id='menupalkki']/div/ul/li[3]/a/span")).click();

//Clicking on Student search option 	 
	 driver.findElement(By.xpath("//*[@id='contentBoxContainer']/div/div/div/div[1]/div/div[1]/ul/li[1]/a")).click();	 

//Entering the last name 	
	 driver.findElement(By.name("lastName")).sendKeys("cris1ss");
	 driver.findElement(By.name("ssnPostfix")).sendKeys("cris1ss");
	 
// Clicking on search button	
	 //((JavascriptExecutor) driver)
   //  .executeScript("window.scrollTo(0, document.body.scrollHeight)");
	 
	 
	 //Select dropdown1 = new Select(driver.findElement(By.id("Search results template")));
		//dropdown1.selectByVisibleText("1A");
		
		Actions actions = new Actions(driver);
     WebElement mainMenu = driver.findElement(By.id("searchReportTemplate")); 
     
     actions.moveToElement(mainMenu);
     actions.click().build().perform();
     actions.sendKeys(Keys.PAGE_DOWN).perform(); 
     actions.sendKeys(Keys.PAGE_DOWN).perform(); 
     actions.sendKeys(Keys.PAGE_DOWN).perform(); 
		
		//((JavascriptExecutor)driver).executeScript("scroll(0,400)");
	    /*JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,400)");*/
	
//Highlighting the save button 
 driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
     
	WebElement Searchbutton= driver.findElement(By.xpath("//input[@value='Search']"));
	Actions act =new Actions(driver);
	act.moveToElement(Searchbutton).perform();

//Clicking on Search button 
	driver.findElement(By.xpath("//input[@value='Search']")).click();

	// driver.findElement(By.xpath("//input[@value='Search']")).click();
	 
	/* Select dropdown1 = new Select(driver.findElement(By.name("searchReportTemplate")));
		dropdown1.selectByVisibleText("oletusraportti (Assumption)");
	
		Actions actions = new Actions(driver);
     WebElement mainMenu = driver.findElement(By.name("searchReportTemplate")); 
     
     actions.moveToElement(mainMenu);
     actions.click().build().perform();
     actions.sendKeys(Keys.PAGE_DOWN).perform(); 
     actions.sendKeys(Keys.PAGE_DOWN).perform(); 
     actions.sendKeys(Keys.PAGE_DOWN).perform(); 
     
     */
     	
  }

}
